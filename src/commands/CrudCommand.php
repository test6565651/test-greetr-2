<?php

namespace Simplexi\Greetr\Commands;

use Illuminate\Console\Command;
use Simplexi\Greetr\Greetr;
use Simplexi\Greetr\Services\ControllerMakerService;
use Simplexi\Greetr\Services\ModelMakerService;
use Simplexi\Greetr\Services\RequestMakerService;
use Simplexi\Greetr\Services\MigrationMakerService;
use Simplexi\Greetr\Services\RouteMakerService;
use Simplexi\Greetr\Services\ViewMakerService;

class CrudCommand extends Command
{
    protected $signature = 'simplexi:crud {name} {columns}';

    protected $description = 'Generate a new model';

    public function handle()
    {
        $name = $this->argument('name');
        $columns = $this->argument('columns');

        $modelMaker = new ModelMakerService($name, $columns);
        $requestMaker = new RequestMakerService($name, $columns);
        $controllerMaker = new ControllerMakerService($name, $columns);
        $migrationMaker = new MigrationMakerService($name, $columns);
        $routeMaker = new RouteMakerService($name, $columns);
        $viewMaker = new ViewMakerService($name, $columns);

        // $greetr = new Greetr($name);
        $this->info("{$name} model generated successfully");
    }
}
